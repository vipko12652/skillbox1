FROM node

RUN mkdir /skillbox_flat
WORKDIr /skillbox_flat
COPY package.json /skillbox_flat

RUN yarn install
COPY . /skillbox_flat
RUN yarn test
RUN yarn build

CMD yarn start

EXPOSE 3000

